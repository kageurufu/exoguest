"""
exoguest shares module

Handles cephfs shares
"""

from .rpc import router

__all__ = ("router",)
