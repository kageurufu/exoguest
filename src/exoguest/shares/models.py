import typing as t
from ..models.pathlib import Mountpoint
import pydantic
import pydantic.validators


class CephAccessRule(pydantic.BaseModel):
    name: str
    key: str


class CephShare(pydantic.BaseModel):
    type: t.Literal["cephfs"]

    name: str
    path: str

    access_rule: t.Optional[CephAccessRule] = pydantic.Field(exclude=["key"])
    mountpoint: Mountpoint = None


CephSharePath = t.Annotated[
    str,
    pydantic.StringConstraints(
        # [host[:port]][,host[:port]...]:[share_path]
        # 169.254.169.254:6789,169.254.169.254:6789:/volumes/my-volume
        pattern=r"^((?:\d+\.\d+\.\d+\.\d+(?::\d+)?)(?:,(?:\d+\.\d+\.\d+\.\d+(?::\d+)?))*):([\/\w\-]+)$"
    ),
]
