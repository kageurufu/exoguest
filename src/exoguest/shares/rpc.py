import pathlib
import typing as t

from openrpc import RPCRouter, Example, ExamplePairing

from ..models.pathlib import FutureMountpoint, Mountpoint
from ..utils.constants import SECURITY_RPC
from .models import CephAccessRule, CephSharePath, CephShare
from ..utils.paths import sanitize_pathname

MOUNT_PATH_SHARES = pathlib.Path("/media/share")

router = RPCRouter()


@router.method(
    "list",
    security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="listing shares",
            params=[Example(name="No parameters", value=None)],
            result=Example(
                name="List of shares",
                value=[
                    {
                        "type": "cephfs",
                        "name": "my-share",
                        "path": "169.254.169.254:/public",
                        "access_rule": {"name": "my-access-rule"},
                        "mountpoint": "/media/volume/my-share",
                    },
                ],
            ),
        )
    ],
)
def list_shares() -> list[CephShare]: ...


@router.method(
    "unmount",
    security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="Unmounting a share",
            params=[
                Example(
                    name="The share to unmount",
                    value={"mountpoint": "/media/share/my-share"},
                )
            ],
            result=Example(name="Unmounted", value=True),
        )
    ],
)
def unmount_share(mountpoint: Mountpoint) -> bool:
    raise NotImplementedError("TODO: mount_ceph:do_unmount")


@router.method(
    "mount_ceph",
    security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="Mounting a cephfs share",
            params=[
                Example(
                    name="An unauthenticated share",
                    value={
                        "share_name": "public-share",
                        "share_path": "169.254.169.254:/public",
                    },
                ),
                Example(
                    name="A share with an access key",
                    value={
                        "share_name": "my-share",
                        "share_path": "169.254.169.254:/public",
                        "access_rule": {"name": "my-access-rule", "key": "abc123"},
                    },
                ),
            ],
            result=Example(
                name="A mounted cephfs share",
                value={
                    "type": "cephfs",
                    "name": "my-share",
                    "path": "169.254.169.254:/public",
                    "access_rule": {"name": "my-access-rule"},
                    "mountpoint": "/media/volume/my-share",
                },
            ),
        )
    ],
)
def mount_ceph(
    *,
    share_name: str,
    share_path: CephSharePath,
    access_rule: t.Optional[CephAccessRule] = None,
    mountpoint: t.Optional[FutureMountpoint] = None,
) -> CephShare:
    """..."""
    if not mountpoint:
        mountpoint = MOUNT_PATH_SHARES / sanitize_pathname(share_name)
        assert not mountpoint.exists(), f"Mountpoint {mountpoint} already exists"

    raise NotImplementedError("TODO: mount_ceph:do_mount")
