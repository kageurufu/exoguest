import logging

from .__version__ import __version__
from .models.settings import Settings

settings = Settings()

logging.basicConfig(level=settings.log.global_level, filename=settings.log.file)
logging.getLogger(__name__).setLevel(level=settings.log.level)
logging.getLogger("aiohttp").setLevel(level=settings.log.aiohttp_level)

settings.save()

__all__ = ("__version__",)
