import typing as t

import pydantic


class AuthRequest(pydantic.BaseModel):
    username: str
    password: str


class AuthResponse(pydantic.BaseModel):
    username: str
    token: str


class Authentication(pydantic.BaseModel):
    username: str
    schemes: t.Optional[dict[str, list[str]]]
