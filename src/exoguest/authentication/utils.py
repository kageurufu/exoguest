import jose.jws

from .. import settings
from .models import Authentication


def encode_token(authentication: Authentication) -> str:
    return jose.jws.sign(
        authentication.model_dump(),
        key=settings.secret_key,
        algorithm="HS256",
    )


def decode_token(token: str) -> Authentication:
    token_data = jose.jws.verify(
        token,
        key=settings.secret_key,
        algorithms=["HS256", "ES256"],
    )

    return Authentication.model_validate_json(token_data)
