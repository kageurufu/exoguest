import typing as t

from openrpc import Depends, RPCRouter, Example, ExamplePairing

from ..models import CallerDetails
from ..utils.constants import TAG_SOCKET_ONLY, SECURITY_RPC
from ..utils.dependencies import session
from . import utils
from .models import Authentication

router = RPCRouter()


def get_authentication(
    _caller_details: CallerDetails,
    session: dict[str, t.Any] = Depends(session),
) -> t.Optional[Authentication]:
    if "authentication" in session:
        return session["authentication"]

    return None


def security_function(
    _caller_details: CallerDetails,
    authentication: t.Optional[Authentication] = Depends(get_authentication),
) -> dict[str, list[str]]:
    if authentication:
        return authentication.schemes

    return {}


@router.method(
    "authenticate",
    tags=[TAG_SOCKET_ONLY],
    examples=[
        ExamplePairing(
            name="Authentication",
            params=[
                Example(
                    name="Auth token",
                    value={"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..."},
                )
            ],
            result=Example(
                name="Authentication",
                value={"username": "exouser", "schemes": {"apikey": ["rpc", "login"]}},
            ),
        )
    ],
)
def authenticate(
    *,
    token: str,
    session: dict[str, t.Any] = Depends(session),
) -> Authentication:
    session["authentication"] = utils.decode_token(token)

    return session["authentication"]


@router.method(
    "me",
    security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="Authentication",
            params=[],
            result=Example(
                name="Authentication",
                value={"username": "exouser", "schemes": {"apikey": ["rpc", "login"]}},
            ),
        )
    ],
)
def me(
    authentication: Authentication = Depends(get_authentication),
) -> Authentication:
    return authentication
