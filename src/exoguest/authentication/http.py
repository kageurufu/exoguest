import simplepam
from aiohttp import typedefs, web

from . import utils
from .models import Authentication, AuthRequest, AuthResponse


async def auth_post_handler(req: web.Request) -> web.Response:
    json = await req.json()
    auth_request = AuthRequest.model_construct(**json)
    if simplepam.authenticate(
        username=auth_request.username,
        password=auth_request.password,
        service="login",
    ):
        authentication = Authentication(
            username=auth_request.username,
            schemes={"apikey": ["login", "rpc"]},
        )
        token = utils.encode_token(authentication)

        return web.json_response(
            AuthResponse(
                username=auth_request.username,
                token=token,
            ).model_dump()
        )
    return web.json_response(
        {"error": "invalid authentication"},
        status=401,
    )


routes = [
    web.post("/auth", auth_post_handler),
]


@web.middleware
async def middleware(request: web.Request, handler: typedefs.Handler) -> web.Response:
    session = request.setdefault("session", {})

    # Check for auth token header
    auth_header = request.headers.get("Authorization", None)
    if auth_header and auth_header.startswith("Bearer "):
        token = auth_header[7:]
        authentication = utils.decode_token(token)

        session["authentication"] = authentication

    return await handler(request)
