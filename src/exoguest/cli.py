import typing as t
import sys
import yaml
import json

import click
from aiohttp import web

from .app import create_app
from .rpc import rpc


@click.group(invoke_without_command=True)
@click.pass_context
def main(ctx: click.Context) -> None:
    if ctx.invoked_subcommand:
        return

    app = create_app()

    web.run_app(app, port=17752)


@main.command()
@click.option("--format", "-f", type=click.Choice(["json", "yaml"]), default="json")
@click.option("--output", "-o", type=click.File("w"), default=sys.stdout)
def discover(format: t.Literal["json", "yaml"], output: t.TextIO) -> None:
    # schema = {"$schema": "https://meta.open-rpc.org", **rpc.discover()}
    schema = rpc.discover()

    if format == "yaml":
        yaml.dump(schema, output, indent=2, sort_keys=False)

    else:
        json.dump(schema, output, indent=2, sort_keys=False)
        output.write("\n")  # Trailing newline for JSON
