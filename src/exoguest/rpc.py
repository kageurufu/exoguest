from openrpc import BearerAuth, Contact, Server

from . import authentication, mounts, notifications, shares, sysload
from .__version__ import __version__
from .utils.monitored_rpc import MonitoredRPCServer

rpc = MonitoredRPCServer(
    title="exosphere-guest-utils",
    description="Exosphere guest utilities RPC",
    version=__version__,
    contact=Contact(
        name="Exosphere",
        url="https://gitlab.com/exosphere/exosphere",
    ),
    servers=[
        Server(name="http", url="http://localhost:17752/rpc"),
        Server(name="websocket", url="ws://localhost:17752/ws"),
    ],
    security_schemes={"apikey": BearerAuth()},
    security_function=authentication.security_function,
    # debug=True,
)

rpc.include_router(authentication.router, prefix="authentication.")
rpc.include_router(notifications.router, prefix="notifications.")
rpc.include_router(sysload.router, prefix="system_load.")
rpc.include_router(mounts.router, prefix="mounts.")
rpc.include_router(shares.router, prefix="shares.")
