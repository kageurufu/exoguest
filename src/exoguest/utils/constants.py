from openrpc import Tag

TAG_SOCKET_ONLY = Tag(
    name="socket-only",
    description="Only available to websockets",
)

SECURITY_RPC = {"apikey": ["rpc"]}
