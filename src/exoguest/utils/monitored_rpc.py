import json
import time
import typing as t

from aiohttp import web
from jsonrpcobjects.objects import ParamsNotification
from openrpc import (
    APIKeyAuth,
    BearerAuth,
    Contact,
    Depends,
    License,
    OAuth2,
    RPCServer,
    Server,
)
from openrpc._discover.discover import get_openrpc_doc

from ..pubsub import PubSubAIOHTTP
from .dependencies import current_websocket


class MonitoredRPCServer(RPCServer):
    _monitor_callback: t.Optional[t.Callable]

    def __init__(
        self,
        title: str | None = None,
        version: str | None = None,
        description: str | None = None,
        terms_of_service: str | None = None,
        contact: Contact | None = None,
        license_: License | None = None,
        servers: list[Server] | Server | None = None,
        security_schemes: dict[str, OAuth2 | BearerAuth | APIKeyAuth] | None = None,
        security_function: (
            t.Callable[..., dict[str, list[str]]]
            | t.Callable[..., t.Awaitable[dict[str, list[str]]]]
            | None
        ) = None,
        *,
        debug: bool = False,
    ) -> None:
        super().__init__(
            title,
            version,
            description,
            terms_of_service,
            contact,
            license_,
            servers,
            security_schemes,
            security_function,
            debug=debug,
        )

        @self.method(name="rpc.monitor")
        def debug_monitor_enable(
            ws: web.WebSocketResponse = Depends(current_websocket),
        ) -> bool:
            PubSubAIOHTTP().listen("rpc.monitor", ws)
            return True

    def discover(self) -> dict[str, t.Any]:
        """Execute "rpc.discover" method defined in OpenRPC spec."""
        methods = [
            method
            for method in self._rpc_methods.values()
            if not method.metadata.name.startswith("rpc.")
        ]
        openrpc = get_openrpc_doc(self._info, methods, self._servers)
        model_dump = openrpc.model_dump(by_alias=True, exclude_unset=True)
        if self.security_schemes and openrpc.components:
            # This is done after OpenRPC model dump rather than before
            # so the security model default values will be kept,
            # `exclude_unset=True` in doc dump would remove them.
            model_dump["components"]["x-securitySchemes"] = {
                name: model.model_dump(exclude_none=True, by_alias=True)
                for name, model in self.security_schemes.items()
            }
        return model_dump

    def process_request(
        self,
        data: bytes | str,
        caller_details: t.Any | None = None,
    ) -> str | None:
        resp = super().process_request(data, caller_details)
        if self.debug:
            pubsub = PubSubAIOHTTP()
            if pubsub.room_active("rpc.monitor"):
                pubsub.broadcast(
                    "rpc.monitor",
                    ParamsNotification(
                        method="rpc.monitor",
                        params={
                            "ts": time.time(),
                            "request": json.loads(data) if data else data,
                            "response": json.loads(resp) if resp else resp,
                        },
                    ).model_dump_json(),
                )

        return resp

    async def process_request_async(
        self,
        data: bytes | str,
        caller_details: t.Any | None = None,
    ):
        resp = await super().process_request_async(data, caller_details)
        if self.debug:
            pubsub = PubSubAIOHTTP()
            if pubsub.room_active("rpc.monitor"):
                pubsub.broadcast(
                    "rpc.monitor",
                    ParamsNotification(
                        method="rpc.monitor",
                        params={
                            "ts": time.time(),
                            "request": json.loads(data) if data else data,
                            "response": json.loads(resp) if resp else resp,
                        },
                    ).model_dump_json(),
                )

        return resp
