import typing as t

from aiohttp import web
from openrpc import Error

from ..models import CallerDetails


def request(
    caller_details: CallerDetails,
) -> web.Request:
    return caller_details["request"]


def session(
    caller_details: CallerDetails,
) -> dict[str, t.Any]:
    return caller_details["request"].setdefault("session", {})


def optional_websocket(
    caller_details: CallerDetails,
) -> t.Optional[web.WebSocketResponse]:
    return caller_details.get("ws", None)


def current_websocket(
    caller_details: CallerDetails,
) -> web.WebSocketResponse:
    if "ws" not in caller_details:
        raise Error(
            code=-32600,
            message="Invalid Request",
            data="Method only available for websocket connections",
        )

    return caller_details["ws"]
