import typing as t


def split_iterable(
    predicate: t.Callable[[t.T], bool],
    iterable: t.Iterable[t.T],
    *,
    before_predicate: bool = True,
) -> t.Iterable[list[t.T]]:
    """
    Split an iterable into chunks, separating on a predicate

    Empty chunks (e.g, where the first element matches the predicate) are not yielded
    """

    chunk = []
    for element in iterable:
        if predicate(element):
            if not before_predicate:
                chunk.append(element)

            if chunk:
                yield chunk
                chunk.clear()

            if before_predicate:
                chunk.append(element)

        else:
            chunk.append(element)

    if chunk:
        yield chunk
