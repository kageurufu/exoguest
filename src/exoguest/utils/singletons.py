import asyncio
import typing as t

TSocket = t.TypeVar("TSocket")


class Singleton(type):
    def __call__(cls, *args, **kwargs):
        if not hasattr(cls, "_instance"):
            cls._instance = super().__call__(args, kwargs)

        return cls._instance


class LoopLocalSingleton(type):
    """
    Metaclass that handles singletons within an asyncio event loop
    """

    def __call__(cls, *args, **kwargs):
        if not hasattr(cls, "_loop_local_instances"):
            cls._loop_local_instances = {}

        loop_id = id(asyncio.get_running_loop())
        if loop_id not in cls._loop_local_instances:
            cls._loop_local_instances[loop_id] = super().__call__(*args, **kwargs)

        return cls._loop_local_instances[loop_id]
