import asyncio
import functools
import importlib.resources
import json
import logging
import pathlib
import typing as t

import yaml
from aiohttp import WSMsgType, web
from aiohttp_middlewares import cors_middleware, error_middleware

from . import authentication
from .models import CallerDetails
from .rpc import rpc

logger = logging.getLogger(__name__)

PUBLIC_PATH = pathlib.Path(importlib.resources.path(__package__, "public"))


async def _websocket_handle_rpc(
    ws: web.WebSocketResponse,
    async_response: t.Coroutine[t.Any, t.Any, t.Optional[str]],
) -> None:
    response = await async_response
    if response is not None:
        await ws.send_str(response)


async def websocket_handler(request: web.Request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    async for msg in ws:
        if msg.type == WSMsgType.ERROR:
            logger.info("ws connection closed with exception %s" % ws.exception())

        elif msg.type == WSMsgType.TEXT:
            asyncio.create_task(
                _websocket_handle_rpc(
                    ws,
                    rpc.process_request_async(
                        msg.data,
                        caller_details=CallerDetails(
                            request=request,
                            ws=ws,
                        ),
                    ),
                )
            )

    logger.debug("websocket %r closed", ws)

    return ws


async def http_handler(request: web.Request):
    request_body = await request.text()
    response = await rpc.process_request_async(
        request_body,
        caller_details=CallerDetails(request=request),
    )

    if response is not None:
        return web.Response(text=response)

    return web.Response()


def serve_file(filename: str):
    async def serve_html(request):
        return web.FileResponse(
            PUBLIC_PATH / filename,
            headers={"content-type": "text/html"},
        )

    return serve_html


async def openrpc_json_handler(request):
    return web.json_response(
        rpc.discover(),
        dumps=functools.partial(json.dumps, indent=2),
    )


async def openrpc_yaml_handler(request):
    return web.Response(
        body=yaml.dump(rpc.discover()), headers={"content-type": "text/plain"}
    )


router = web.UrlDispatcher()
router.add_routes(authentication.routes)
router.add_routes(
    [
        web.post("/rpc", http_handler),
        web.get("/ws", websocket_handler),
        web.get("/openrpc.json", openrpc_json_handler),
        web.get("/openrpc.yml", openrpc_yaml_handler),
        web.get("/", serve_file("index.html")),
        web.get("/monitor", serve_file("monitor.html")),
        web.static("/", PUBLIC_PATH),
    ]
)

middleware = [
    cors_middleware(allow_all=True),
    error_middleware(),
    authentication.middleware,
]
