import pathlib
import subprocess
import typing as t

from ..utils.iterables import split_iterable
from .models import OpenFileInfo


def iter_open_files(path: t.Union[str, pathlib.Path]) -> t.Iterable[OpenFileInfo]:
    """
    Parses the output of `lsof -F pfcn` to find any open files within a path

    `-F` is "Output for processing by other programs"
    `pfcn` selects the pid, command, fd, and filename for output

    lsof outputs the following for each open file
        p[pid]
        c[command]
        f[fd]
        n[file_path]

    Note: f/n may not be included in some cases
    """

    lsof = subprocess.run(
        ("lsof", "-F", "pcfn", str(path)),
        capture_output=True,
        check=False,
    )

    output_lines = lsof.stdout.decode().splitlines()

    for open_file_lines in split_iterable(lambda x: x.startswith("p"), output_lines):
        open_file_data = {line[0]: line[1:] for line in open_file_lines}

        yield OpenFileInfo(
            pid=int(open_file_data.get("p", "-1")),
            command=open_file_data.get("c", "[unknown]"),
            fd=open_file_data.get("f", None),
            name=open_file_data.get("n", None),
        )
