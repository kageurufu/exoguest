import pathlib
import re
import subprocess

from openrpc import Example, ExamplePairing, RPCRouter

from ..models.pathlib import BlockDevice, FutureMountpoint, Mountpoint
from ..utils.constants import SECURITY_RPC
from .models import MountInfo, OpenFileInfo
from .utils import iter_open_files

MOUNT_PATH_VOLUMES = pathlib.Path("/media/volume")

router = RPCRouter()

FSTAB_RE = re.compile(
    r"^(?P<spec>\S+)"
    r" (?P<file>\S+)"
    r" (?P<vfstype>\S+)"
    r" (?P<mntops>.+)"
    r" (?P<freq>\d+)"
    r" (?P<passno>\d+)$"
)


@router.method(
    "list",
    security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="Listing mounted devices",
            params=[],
            result=Example(
                name="Mounted devices",
                value=[
                    {
                        "spec": "/dev/sda1",
                        "file": "/",
                        "vfstype": "ext4",
                        "mntops": "rw,noatime,discard",
                        "freq": "0",
                        "passno": "0",
                    },
                    {
                        "spec": "169.254.169.254:6789:/volumes/my-share",
                        "file": "/media/share/my-share",
                        "vfstype": "ceph",
                        "mntops": "rw,noatime,name=me,secret=<hidden>,fsid=my-share,acl",
                        "freq": "0",
                        "passno": "0",
                    },
                    {
                        "spec": "/dev/sdb",
                        "file": "/media/volume/sdb",
                        "vfstype": "ext4",
                        "mntops": "rw,nosuid,nodev,relatime",
                        "freq": "0",
                        "passno": "0",
                    },
                ],
            ),
        )
    ],
)
def list_mounts() -> list[MountInfo]:
    with open("/proc/mounts") as fp:
        results = []
        for line in fp:
            if not line.strip():
                continue

            match = FSTAB_RE.search(line.strip())
            if match:
                results.append(MountInfo(**match.groupdict()))

        return results


@router.method(
    "open_files",
    # security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="Listing open files",
            params=[
                Example(
                    name="A share", value={"mountpoint": "/media/share/frank-share"}
                )
            ],
            result=Example(
                name="Open files on my-share",
                value=[
                    {
                        "pid": 190876,
                        "command": "bash",
                        "fd": "cwd",
                        "name": "/media/share/frank-share",
                    },
                    {
                        "pid": 194185,
                        "command": "vim",
                        "fd": "3",
                        "name": "/media/share/frank-share/share-test/.test-2.txt.swp",
                    },
                ],
            ),
        )
    ],
)
def list_open_files(*, mountpoint: Mountpoint) -> list[OpenFileInfo]:
    return list(iter_open_files(mountpoint))


@router.method(
    security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="Unmounting a device",
            params=[
                Example(
                    name="A named volume",
                    value={"mountpoint": "/media/volume/my-volume"},
                )
            ],
            result=Example(name="Unmounted", value=True),
        )
    ],
)
def unmount(
    *,
    mountpoint: Mountpoint,
) -> bool:
    res = subprocess.check_output(("umount", str(mountpoint)))

    try:
        mountpoint.rmdir()
    except OSError:
        pass

    return res


@router.method(
    security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="Mounting a volume",
            params=[
                Example(
                    name="Example",
                    value={
                        "device": "/dev/sdb",
                        "mountpoint": "/media/volume/my-volume",
                    },
                )
            ],
            result=Example(
                name="The mounted volume",
                value={
                    "spec": "/dev/sdb",
                    "file": "/media/volume/my-volume",
                    "vfstype": "ext4",
                    "mntops": "rw,noatime,discard",
                    "freq": "0",
                    "passno": "0",
                },
            ),
        )
    ],
)
def mount(
    *,
    device: BlockDevice,
    mountpoint: FutureMountpoint,
) -> MountInfo:
    """Attempt to mount a block device to a path"""
    # TODO: Format the device if unformatted
    mountpoint.mkdir(parents=True, exist_ok=True)
    subprocess.check_output(
        (
            "mount",
            f"--source={device}",
            f"--target={mountpoint}",
        )
    )
