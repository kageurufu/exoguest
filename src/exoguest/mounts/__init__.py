"""
exoguest mounts module

Handles volume mounting status, etc
"""

from .rpc import router

__all__ = ("router",)
