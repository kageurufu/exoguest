import typing as t

import pydantic


class MountInfo(pydantic.BaseModel):
    spec: str
    file: str
    vfstype: str
    mntops: str
    freq: str
    passno: str


class OpenFileInfo(pydantic.BaseModel):
    pid: int
    command: str
    fd: t.Optional[str]
    name: t.Optional[str]
