function log(message) {
  const elem = document.getElementById("log");

  elem.appendChild(document.createTextNode("\n" + message));
}

function logJson(jsonData) {
  const elem = document.getElementById("log");

  const jv = document.createElement("json-viewer");
  jv.innerText = JSON.stringify(jsonData);
  elem.appendChild(jv);

  return jv;
}

const url = new URL(document.location);
url.pathname = "/ws";
url.protocol = document.location.protocol === "https:" ? "wss:" : "ws:";
log("!url: " + url);

const ws = new WebSocket(url);
const formatRpc = (method, params = undefined, id = undefined) => {
  if (id === undefined) {
    id = crypto.randomUUID();
  }
  return [
    id,
    JSON.stringify({
      method,
      params,
      id,
      jsonrpc: "2.0",
    }),
  ];
};

const rpc = (ws, method, params) => {
  const [id, command] = formatRpc(method, params);
  const rv = logJson({ method, params });

  let handler;

  const promise = new Promise((resolve) => {
    handler = (d) => {
      let response = JSON.parse(d.data);
      if (response.id === id) {
        resolve(response);
      }
    };

    ws.addEventListener("message", handler);
    ws.send(command);
  })
    .then((r) => {
      rv.data = { ...rv.data, result: r.result };
      return r;
    })
    .finally(() => {
      ws.removeEventListener("message", handler);
    });

  return promise;
};

function handleDiscovery({ result }) {
  console.log(result);

  for (const method of result.methods) {
    console.log(method);
    const option = document.createElement("option");
    option.value = JSON.stringify({
      method: method.name,
      params:
        method.params?.length > 0
          ? Object.fromEntries(
              method.params.map(({ name, schema }) => [
                name,
                schema.type === "array" ? [] : null,
              ])
            )
          : undefined,
    });

    historyList.appendChild(option);
  }
}

ws.addEventListener("close", () => {
  log("!close");
});

ws.addEventListener("open", (evt) => {
  console.log("open", evt);
  log("!open");

  rpc(ws, "notifications.subscribe", { channels: ["notify.system_load"] });
  rpc(ws, "rpc.discover").then(handleDiscovery);
});
ws.addEventListener("error", () => {
  log("!error");
});
ws.addEventListener("message", (d) => {
  console.log("data", d);
});

const form = document.getElementById("command-form");
const historyList = document.getElementById("history");
const messageInput = document.getElementById("message");
const submitButton = document.getElementById("submit");

form.addEventListener("submit", function (e) {
  e.preventDefault();

  const request = JSON.parse(messageInput.value);
  if (request.method) {
    const newOption = historyList.appendChild(document.createElement("option"));
    newOption.value = messageInput.value;

    rpc(ws, request.method, request.params);

    messageInput.value = "";
  }
});
