from aiohttp import web
from openrpc import Depends, Example, ExamplePairing, RPCRouter

from .pubsub import PubSubAIOHTTP
from .utils.dependencies import current_websocket

router = RPCRouter()


@router.method(
    "list",
    examples=[
        ExamplePairing(
            name="listing notifications",
            params=[],
            result=Example(
                name="a list of notifications",
                value=["notify.system_load"],
            ),
        )
    ],
    tags=["socket-only"],
)
def get_list(
    ws: web.WebSocketResponse = Depends(current_websocket),
) -> list[str]:
    return list(
        room for room in PubSubAIOHTTP().list_rooms() if room.startswith("notify.")
    )


@router.method(
    examples=[
        ExamplePairing(
            name="subscribing to notifications",
            params=[
                Example(
                    name="Subscribing to system load",
                    value={"channels": ["notify.system_load"]},
                )
            ],
            result=Example(name="an empty response", value=[]),
        )
    ],
    tags=["socket-only"],
)
def subscribe(
    channels: list[str],
    ws: web.WebSocketResponse = Depends(current_websocket),
) -> None:
    for channel in channels:
        PubSubAIOHTTP().listen(channel, ws)
