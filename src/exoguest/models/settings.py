import yaml
import pydantic
import pathlib
import os
import binascii
import pydantic_settings
import typing as t

LogLevel = t.Literal["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"]


class LogSettings(pydantic.BaseModel):
    file: str | None = None

    level: LogLevel = "INFO"
    aiohttp_level: LogLevel = "INFO"
    global_level: LogLevel = "WARNING"


class Settings(pydantic_settings.BaseSettings):
    model_config = pydantic_settings.SettingsConfigDict(yaml_file="config.yml")

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: type[pydantic_settings.BaseSettings],
        init_settings: pydantic_settings.PydanticBaseSettingsSource,
        env_settings: pydantic_settings.PydanticBaseSettingsSource,
        dotenv_settings: pydantic_settings.PydanticBaseSettingsSource,
        file_secret_settings: pydantic_settings.PydanticBaseSettingsSource,
    ) -> tuple[pydantic_settings.PydanticBaseSettingsSource, ...]:
        return (
            dotenv_settings,
            env_settings,
            pydantic_settings.YamlConfigSettingsSource(settings_cls),
            file_secret_settings,
            init_settings,
        )

    @pydantic.field_serializer("data_path")
    def serialize_path(path: pathlib.Path):
        return str(path.absolute())

    @pydantic.validator("data_path")
    def relative_paths(path: pathlib.Path):
        root = pathlib.Path().absolute()
        if path.is_relative_to(root):
            return path.relative_to(root)
        return path

    def save(self) -> None:
        with open(self.model_config.get("yaml_file"), "w") as fp:
            yaml.dump(self.model_dump(exclude_defaults=True), fp, sort_keys=False)

    secret_key: str = pydantic.Field(
        default_factory=lambda: binascii.hexlify(os.urandom(32))
    )

    data_path: pathlib.Path = pathlib.Path(".data")

    log: LogSettings = LogSettings()
