import pathlib
import textwrap
import typing as t

import pydantic


def _validate_path_is_block_device(p: pathlib.Path):
    assert p.is_block_device(), "Path must be a device"
    return p


def _validate_path_is_mountpoint(p: pathlib.Path):
    assert p.is_mount(), "Path must be a mountpoint"
    return p


def _validate_path_is_directory(p: pathlib.Path):
    assert p.is_dir(), "Path must be a directory"
    return p


def _validate_path_is_file(p: pathlib.Path):
    assert p.is_file(), "Path must be a file"
    return p


def _validate_path_exists(p: pathlib.Path):
    assert p.exists(), "Path must exist"
    return p


def _validate_path_is_mountable(p: pathlib.Path):
    """
    A path is considered mountable if any of:

    * It is an empty directory
      * And not a mount
    * It does not exist
      * And no parent exists that is not a directory
    """
    if p.is_dir():
        assert not p.is_mount(), f"{p} is already a mountpoint"
        assert not any(
            p.iterdir()
        ), f"{p} is a directory with contents, and cannot be a mountpoint"

    else:
        assert not p.exists(), f"{p} is a {categorize(p)}"
        for parent in p.parents:
            assert (
                parent.is_dir() or not parent.exists()
            ), f"{parent} is not a valid mount path, it is a {categorize(parent)}"

    return p


def categorize(p: pathlib.Path) -> t.Optional[str]:
    if p.is_char_device():
        return "char_device"
    if p.is_dir():
        return "dir"
    if p.is_file():
        return "file"
    if p.is_mount():
        return "mount"
    if p.is_symlink():
        return "symlink"
    if p.is_block_device():
        return "block_device"
    if p.is_char_device():
        return "char_device"
    if p.is_fifo():
        return "fifo"
    if p.is_socket():
        return "socket"


BlockDevice = t.Annotated[
    pathlib.Path,
    pydantic.AfterValidator(_validate_path_is_block_device),
    pydantic.WithJsonSchema(
        {
            "type": "string",
            "description": "A block device",
            "examples": ["/dev/sdb"],
        }
    ),
]
Mountpoint = t.Annotated[
    pathlib.Path,
    pydantic.AfterValidator(_validate_path_is_mountpoint),
    pydantic.WithJsonSchema(
        {
            "type": "string",
            "description": "A mountpoint",
            "examples": ["/", "/media/volume/my-volume"],
        }
    ),
]
Directory = t.Annotated[
    pathlib.Path,
    pydantic.AfterValidator(_validate_path_is_directory),
    pydantic.WithJsonSchema(
        {
            "type": "string",
            "description": "An existing directory",
            "examples": ["/home/exouser"],
        }
    ),
]
File = t.Annotated[
    pathlib.Path,
    pydantic.AfterValidator(_validate_path_is_file),
    pydantic.WithJsonSchema(
        {
            "type": "string",
            "description": "A file",
            "examples": ["/home/exouser/some-file.txt"],
        }
    ),
]
Path = t.Annotated[
    pathlib.Path,
    pydantic.AfterValidator(_validate_path_exists),
    pydantic.WithJsonSchema(
        {
            "type": "string",
            "description": "A path",
            "examples": ["/home/exouser/", "/home/exouser/some-file.txt"],
        }
    ),
]

FutureMountpoint = t.Annotated[
    pathlib.Path,
    pydantic.AfterValidator(_validate_path_is_mountable),
    pydantic.WithJsonSchema(
        {
            "type": "string",
            "description": (
                "A mountable path\n"
                + textwrap.dedent(_validate_path_is_mountable.__doc__)
            ),
            "examples": ["/media/volume/my-volume"],
        }
    ),
]
