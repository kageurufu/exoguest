import typing as t

from aiohttp import web


class CallerDetails(t.TypedDict):
    request: web.Request
    ws: t.Optional[web.WebSocketResponse] = None
