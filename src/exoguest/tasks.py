from aiohttp import web
import asyncio
import contextlib
import datetime
import functools
import logging
import time
import typing as t

from jsonrpcobjects.objects import Notification, ParamsNotification

from .pubsub import PubSubAIOHTTP

logger = logging.getLogger(__name__)

registered_tasks: list[t.Callable[[web.Application], t.Generator]] = []


def register_task(
    task: t.Callable[[web.Application], t.Generator],
) -> t.Callable[[web.Application], t.Generator]:
    registered_tasks.append(task)
    return task


def background_task(func: t.Callable[[], t.Coroutine]) -> t.Callable[[], t.Coroutine]:
    @functools.wraps(func)
    async def _task(app):
        logger.debug("Starting background task %r", func)

        task = asyncio.create_task(func())

        yield

        task.cancel()
        with contextlib.suppress(asyncio.CancelledError):
            await task

    register_task(_task)

    return func


def recurring_task(
    *,
    every: t.Union[datetime.timedelta, int, float],
) -> t.Callable[[t.Callable[[], t.T]], t.Callable[[], t.T]]:
    if isinstance(every, datetime.timedelta):
        every = every.total_seconds()

    def decorator(func):
        @background_task
        @functools.wraps(func)
        async def reccurrable() -> None:
            logger.debug("Scheduling %r every %ds", func, every)

            try:
                while True:
                    await asyncio.sleep(every)
                    logger.debug("executing scheduled task %r", func)
                    res = func()
                    if isinstance(res, t.Awaitable):
                        await res

            except asyncio.CancelledError:
                logger.info("task %r cancelled", func)
                return

        return func

    return decorator


async def send_notifications(
    notification: t.Union[Notification, ParamsNotification],
) -> None:
    pubsub = PubSubAIOHTTP()

    pubsub.broadcast(
        notification.method,
        notification.model_dump_json(),
    )

    if pubsub.room_active("rpc.monitor"):
        pubsub.broadcast(
            "rpc.monitor",
            ParamsNotification(
                method="rpc.monitor",
                params={"ts": time.time(), "notification": notification},
            ).model_dump_json(),
        )
