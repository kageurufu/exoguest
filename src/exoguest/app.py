from aiohttp import web

from . import http, tasks


def create_app():
    app = web.Application(
        router=http.router,
        middlewares=http.middleware,
    )

    app.cleanup_ctx.extend(tasks.registered_tasks)

    return app
