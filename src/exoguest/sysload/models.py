import typing as t

import pydantic
from jsonrpcobjects.objects import ParamsNotification


class SystemLoad(pydantic.BaseModel):
    epoch: int

    cpuPctUsed: int
    memPctUsed: int
    rootfsPctUsed: int
    gpuPctUsed: t.Optional[int]
    diskUsage: dict[str, int]


class SystemLoadNotification(ParamsNotification):
    params: SystemLoad
