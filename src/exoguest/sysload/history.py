import collections
import datetime
import json
import logging

from .. import settings
from ..tasks import register_task, recurring_task, send_notifications
from .models import SystemLoad, SystemLoadNotification
from .utils import get_system_load

PERIOD = datetime.timedelta(seconds=30)
HISTORICAL_PERIOD = datetime.timedelta(hours=1)
HISTORY_FILE = settings.data_path / "history.jsonl"

logger = logging.getLogger(__name__)

# Store one hour of history
history: collections.deque[SystemLoad] = collections.deque(
    maxlen=int(HISTORICAL_PERIOD / PERIOD)
)


@register_task
async def load_history(_app):
    if HISTORY_FILE.exists():
        with HISTORY_FILE.open("r") as fp:
            for line in fp:
                history.append(SystemLoad(**json.loads(line)))

        logger.info("Read %d historical entries from %s", len(history), HISTORY_FILE)

    yield

    save_history()


@recurring_task(every=PERIOD)
async def notify_system_load() -> None:
    method_name = "notify.system_load"

    system_load = get_system_load()
    history.append(system_load)

    await send_notifications(
        SystemLoadNotification(
            method=method_name,
            params=system_load,
        )
    )


@recurring_task(every=PERIOD * 10)
def save_history() -> None:
    logger.info("Saving %d entries to %s", len(history), HISTORY_FILE)
    with HISTORY_FILE.open("w") as fp:
        for entry in history:
            fp.write(entry.model_dump_json() + "\n")
