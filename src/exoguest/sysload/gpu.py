import atexit

import pynvml

pynvml.nvmlInit()
atexit.register(pynvml.nvmlShutdown)


def get_gpu_load():
    device_count = pynvml.nvmlDeviceGetCount()
    if device_count < 1:
        return
    device = pynvml.nvmlDeviceGetHandleByIndex(0)
    stats = pynvml.nvmlDeviceGetUtilizationRates(device)
    return int(stats.gpu)
