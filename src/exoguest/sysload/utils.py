import pathlib
import time

import psutil

from .gpu import get_gpu_load
from .models import SystemLoad


def get_system_load() -> SystemLoad:
    return SystemLoad(
        epoch=int(time.time()),
        cpuPctUsed=int(psutil.cpu_percent()),
        memPctUsed=int(psutil.virtual_memory().percent),
        rootfsPctUsed=int(psutil.disk_usage(str(pathlib.Path("/"))).percent),
        gpuPctUsed=get_gpu_load(),
        diskUsage={
            p.mountpoint: int(psutil.disk_usage(p.mountpoint).percent)
            for p in psutil.disk_partitions()
        },
    )
