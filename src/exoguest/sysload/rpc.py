from openrpc import Example, ExamplePairing, RPCRouter

from ..utils.constants import SECURITY_RPC
from .history import history
from .models import SystemLoad
from .utils import get_system_load

router = RPCRouter()


@router.method(
    security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="basic system load",
            params=[],
            result=Example(
                name="System load information",
                value={
                    "epoch": 1709224179,
                    "cpuPctUsed": 12,
                    "memPctUsed": 24,
                    "rootfsPctUsed": 16,
                    "gpuPctUsed": None,
                    "diskUsage": {
                        "/": 16,
                        "/boot/efi": 5,
                        "/media/volume/my-volume": 0,
                    },
                },
            ),
        )
    ],
)
def current() -> SystemLoad:
    return get_system_load()


@router.method(
    security=SECURITY_RPC,
    examples=[
        ExamplePairing(
            name="Historical system load",
            params=[],
            result=Example(
                name="Abbreviated result",
                description="Up to one hour of results would be returned",
                value=[
                    {
                        "epoch": 1709224179,
                        "cpuPctUsed": 12,
                        "memPctUsed": 24,
                        "rootfsPctUsed": 16,
                        "gpuPctUsed": None,
                        "diskUsage": {
                            "/": 16,
                            "/boot/efi": 5,
                            "/media/volume/my-volume": 0,
                        },
                    }
                ],
            ),
        )
    ],
)
def historical() -> list[SystemLoad]:
    return list(history)
