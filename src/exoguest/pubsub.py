import abc
import asyncio
import collections
import typing as t
import weakref

from aiohttp import web

from .utils.singletons import LoopLocalSingleton

TSocket = t.TypeVar("TSocket")


class _PubSub(t.Generic[TSocket], metaclass=LoopLocalSingleton):
    """
    Super simple websocket pub/sub handler for asyncio.
    """

    _rooms: dict[str, weakref.WeakSet[TSocket]]

    def __init__(self) -> None:
        self._rooms = collections.defaultdict(weakref.WeakSet)

    def list_rooms(self):
        return list(self._rooms.keys())

    def room_active(self, room: str):
        return len(self._rooms[room]) > 0

    def listen(self, room: str, ws: TSocket) -> None:
        self._rooms[room].add(ws)

    def broadcast(self, room: str, message: str) -> None:
        loop = asyncio.get_running_loop()

        for ws in self._rooms[room]:
            loop.create_task(self._send(ws, message))

    @abc.abstractmethod
    async def _send(self, ws: TSocket, message: str) -> t.NoReturn:
        raise NotImplementedError()


class PubSubAIOHTTP(_PubSub[web.WebSocketResponse]):
    async def _send(self, ws, message) -> None:
        try:
            await ws.send_str(message)
        except ConnectionResetError:
            pass
