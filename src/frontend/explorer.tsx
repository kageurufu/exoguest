import { useCallback, useEffect, useRef, useState } from "react";
import { createRoot } from "react-dom/client";
import { JSONTree, KeyPath } from "react-json-tree";
import useWebSocket, { ReadyState } from "react-use-websocket";
import { useImmer } from "use-immer";
import { OpenRpc } from "./openrpc";

function RPCExplorer({ url }: { url: string }) {
  const { sendJsonMessage, lastJsonMessage, readyState } =
    useWebSocket<Record<string, unknown>>(url);

  const [schema, setSchema] = useState<OpenRpc>();
  const [entries, setEntries] = useImmer<Array<Record<string, unknown>>>([]);

  useEffect(() => {
    if (readyState === ReadyState.OPEN) {
      const request = {
        jsonrpc: "2.0",
        method: "rpc.discover",
        id: "rpc.discover",
      };

      sendJsonMessage(request);
      setEntries((e) => e.concat(request));
    }
  }, [sendJsonMessage, readyState]);

  useEffect(() => {
    if (lastJsonMessage == null) return;
    if (lastJsonMessage.id === "rpc.discover") {
      setSchema(lastJsonMessage.result as OpenRpc);
    }

    setEntries((entries) => {
      if (lastJsonMessage?.id) {
        let entry = entries.find((e) => e.id == lastJsonMessage.id);
        if (entry) {
          entry.result = lastJsonMessage.result;
          return entries;
        }
      }
      return entries.concat(lastJsonMessage);
    });
  }, [setEntries, lastJsonMessage]);

  const [formState, setFormState] = useImmer<
    Record<string, string> & { method?: string }
  >({});

  const rpc = (method: string, params?: any, id?: string | number) => {
    if (id == null) {
      id = crypto.randomUUID();
    }
    const request = { jsonrpc: "2.0", method, params };
    sendJsonMessage(request);
  };
  const sendRequest = (req: { [x: string]: unknown }) => {
    sendJsonMessage(req);
    setEntries((p) => p.concat(req));
  };

  const selectedMethod = schema?.methods?.find(
    (m) => m.name == formState.method
  );

  return (
    <div>
      <h3>
        ExoGuest Explorer
        <small style={{ float: "right", opacity: 0.6 }}>
          {readyState === 0
            ? "CONNECTING"
            : readyState === 1
              ? "OPEN"
              : readyState === 2
                ? "CLOSING"
                : readyState === 3
                  ? "CLOSED"
                  : ""}
        </small>
      </h3>

      {schema ? (
        <form
          onSubmit={(ev: React.FormEvent<HTMLFormElement>) => {
            ev.preventDefault();

            const { method, ...params } = formState;
            sendRequest({
              jsonrpc: "2.0",
              id: crypto.randomUUID(),
              method: formState.method,
              params: Object.fromEntries(
                Object.entries(params).map(([k, v]) => [k, JSON.parse(v)])
              ),
            });
          }}
        >
          <select
            name="method"
            value={formState.method ?? undefined}
            onChange={(ev) => setFormState({ method: ev.target.value })}
          >
            <option disabled selected={formState.method == null}>
              Select a Method
            </option>
            {schema.methods.map((method) => (
              <option key={method.name} value={method.name}>
                {method.name}
              </option>
            ))}
          </select>

          {selectedMethod?.params?.map((p) => (
            <label>
              {p.name} ({p.schema?.type})
              <input
                key={p.name}
                type="text"
                onChange={(e) =>
                  setFormState((s) => {
                    s[p.name] = e.target.value;
                  })
                }
              />
            </label>
          ))}

          <button type="submit">Send!</button>
        </form>
      ) : null}

      <JSONTree data={selectedMethod} />
      <JSONTree data={formState} />

      <code>
        {entries.map((entry, idx) => (
          <JSONTree
            key={idx}
            hideRoot
            getItemString={(
              nodeType: string,
              data: unknown,
              itemType: React.ReactNode,
              itemString: string,
              keyPath: KeyPath
            ) => {
              let str = JSON.stringify(data);
              if (str.length > 90) {
                return str.substring(0, 90) + "...";
              }
              return str;
            }}
            data={Object.fromEntries(
              Object.entries(entry).filter(
                ([k, v]) => k !== "jsonrpc" && k !== "id"
              )
            )}
          />
        ))}
      </code>
    </div>
  );
}

const url = new URL("/ws", document.location.href);
url.protocol = url.protocol === "https:" ? "wss:" : "ws:";

createRoot(document.getElementById("app")!).render(
  <RPCExplorer url={url.toString()} />
);
