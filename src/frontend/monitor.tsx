import { useCallback, useEffect, useRef, useState } from "react";
import { createRoot } from "react-dom/client";
import { JSONTree, KeyPath } from "react-json-tree";

function RPCMonitor() {
  const [socket, setSocket] = useState<WebSocket>();
  const [entries, setEntries] = useState<Array<unknown>>([]);

  const onOpen = useCallback((ev: Event) => {
    console.log(ev);
    (ev.currentTarget as WebSocket).send(
      JSON.stringify({
        jsonrpc: "2.0",
        method: "rpc.monitor",
      })
    );
  }, []);
  const onClose = useCallback((ev: Event) => {
    console.log(ev);
    setTimeout(() => setSocket(undefined), 2000);
  }, []);
  const onError = useCallback((ev: Event) => {
    console.log(ev);
    setTimeout(() => setSocket(undefined), 2000);
  }, []);
  const onMessage = useCallback((ev: MessageEvent<string>) => {
    console.log(ev);
    let entry = JSON.parse(ev.data).params;

    setEntries((entries) => [
      ...entries,
      entry.notification != null
        ? {
            method: entry.notification.method,
            params: entry.notification.params,
          }
        : {
            method: entry.request.method,
            params: entry.request.params,
            result: entry.response?.result,
          },
    ]);
  }, []);

  useEffect(() => {
    if (socket == null) {
      const url = new URL("/ws", document.location.href);
      url.protocol = url.protocol === "https:" ? "wss:" : "ws:";
      const ws = new WebSocket(url);
      ws.addEventListener("open", onOpen);
      ws.addEventListener("close", onClose);
      ws.addEventListener("error", onError);
      ws.addEventListener("message", onMessage);
      setSocket(ws);
    }
  }, [socket]);

  return (
    <div>
      <h3>
        Monitor
        <small style={{ float: "right", opacity: 0.6 }}>
          {socket?.readyState === 0
            ? "CONNECTING"
            : socket?.readyState === 1
              ? "OPEN"
              : socket?.readyState === 2
                ? "CLOSING"
                : socket?.readyState === 3
                  ? "CLOSED"
                  : ""}
        </small>
      </h3>

      <code>
        {entries.map((entry, idx) => (
          <JSONTree
            key={idx}
            hideRoot
            getItemString={(
              nodeType: string,
              data: unknown,
              itemType: React.ReactNode,
              itemString: string,
              keyPath: KeyPath
            ) => {
              let str = JSON.stringify(data);
              if (str.length > 90) {
                return str.substring(0, 90) + "...";
              }
              return str;
            }}
            data={entry}
          />
        ))}
      </code>
    </div>
  );
}

createRoot(document.getElementById("app")!).render(<RPCMonitor />);
