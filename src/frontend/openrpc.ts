export interface OpenRpc {
  openrpc: string;
  info: Info;
  methods: Array<Method>;

  servers?: Array<Server>;
  components?: Array<{}>;
  externalDocs?: ExternalDocs;
}

interface Info {
  title: string;
  version: string;
  description?: string;
  termsOfService?: string;
  contact?: { name?: string; url?: string; email?: string };
  license?: { name: string; url?: string };
}

interface Server {
  name: string;
  url: string;
  summary?: string;
  description?: string;
  variables?: Record<
    string,
    { default: string; description?: string; enum?: Array<string> }
  >;
}
interface ExternalDocs {
  url: string;
  description?: string;
}

interface Tag {
  name: string;
  summary?: string;
  description?: string;
  externalDocs?: ExternalDocs;
}

interface Component {
  contentDescriptors?: Record<string, ContentDescriptor>; //	An object to hold reusable Content Descriptor Objects.
  schemas?: Record<string, Schema>; //An object to hold reusable Schema Objects.
  examples?: Record<string, Example>; //	An object to hold reusable Example Objects.
  links?: Record<string, Link>; //An object to hold reusable Link Objects.
  errors?: Record<string, Error>; //	An object to hold reusable Error Objects.
  examplePairingObjects?: Record<string, ExamplePairingObject>; //	An object to hold reusable Example Pairing Objects.
  tags?: Record<string, Tag>; //An object to hold reusable Tag Objects.
}

interface Reference {
  ["$ref"]: string;
}

interface ContentDescriptor {}
interface Schema {}
interface Example {}
interface Link {}
interface Error {}
interface ExamplePairingObject {}

interface Method {
  name: string;
  tags?: Array<Tag>;
  summary?: string;
  description?: string;
  externalDocs?: ExternalDocs;
  params?: Array<MethodParam>;
}

interface MethodParam {}
